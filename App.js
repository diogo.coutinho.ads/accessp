import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import firebase from 'firebase';

import Router from './Router';

const customTextProps = {
  style: {
    fontFamily: 'roboto-regular'
  }
};

console.disableYellowBox = true;

class App extends Component {

  
  componentWillMount () {
    //Posso Fazer qualquer tipo de configuração global aqui como por exemplo o Firebase
    if (firebase.apps.length === 0) {
      firebase.initializeApp({
        apiKey: "AIzaSyC8Ol-vgrnws-VKWXQeiyxBsfWJlH1YOqI",
        authDomain: "accessp-246ad.firebaseapp.com",
        databaseURL: "https://accessp-246ad.firebaseio.com",
        projectId: "accessp-246ad",
        storageBucket: "accessp-246ad.appspot.com",
        messagingSenderId: "796013099751"
      })
    }
    
  }

  render() {
    return (
      <Router></Router>
    );
  }
}

export default App